package br.ufal.ic.projectweb.configurations;

import br.ufal.ic.projectweb.interceptors.AutorizadorInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    //
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //  AutorizadorInterceptor apply to all URLs.
        registry.addInterceptor(new AutorizadorInterceptor());
    }

}