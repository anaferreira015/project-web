package br.ufal.ic.projectweb.repositories;

import br.ufal.ic.projectweb.models.Administrador;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdministradorRepository  extends JpaRepository<Administrador, Integer> {

}
