package br.ufal.ic.projectweb.repositories;

import br.ufal.ic.projectweb.models.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AlunoRepository extends JpaRepository<Aluno, Integer>{

    @Override
    Optional<Aluno> findById(Integer integer);
}
