package br.ufal.ic.projectweb;

import br.ufal.ic.projectweb.models.Administrador;
import br.ufal.ic.projectweb.repositories.AdministradorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(DataLoader.class);

    @Autowired
    private AdministradorRepository administradorRepository;

    @Override
    public void run(String... arg0) throws Exception {
        logger.info("Carregando dados de teste na inicialização...");

        this.carregarDadosPessoa();

        logger.info("Pessoas carregadas: {}", administradorRepository.count());

    }

    public void carregarDadosPessoa() {
        Administrador administrador1 = new Administrador();
        administrador1.setCod(1);
        administrador1.setLogin("lucas");
        administrador1.setSenha("123");

        Administrador administrador2 = new Administrador();
        administrador2.setCod(2);
        administrador2.setLogin("lucas");
        administrador2.setSenha("123");

        Administrador administrador3 = new Administrador();
        administrador3.setCod(3);
        administrador3.setLogin("lucas");
        administrador3.setSenha("123");
    }
}
