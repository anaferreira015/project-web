package br.ufal.ic.projectweb.controllers;

import br.ufal.ic.projectweb.models.Administrador;
import br.ufal.ic.projectweb.repositories.AlunoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    AlunoRepository alunoRepository;

    Administrador adiministrador = new Administrador();

    @RequestMapping("loginForm")
    public String loginForm() {
        return "index";
    }

    @RequestMapping("efetuaLogin")
    public String efetuaLogin(Administrador administrador, HttpSession session) {
        /*new JdbcUsuarioDao().existeUsuario(usuario)*/
        if(alunoRepository.findById(adiministrador.getCod()).isPresent()) {
            session.setAttribute("usuarioLogado", administrador);
            return "menu";
        }
        return "redirect:loginForm";
    }

    @RequestMapping("logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:loginForm";
    }


}
